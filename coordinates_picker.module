<?php

/**
 * @file
 * Code for the module coordinates_picker.
 */

/**
 * Implements hook_permission().
 */
function coordinates_picker_permission() {
  return array(
    'administer coordinates_picker' => array(
      'title' => t('Administer coordinates picker module.'),
      'description' => t('Administer the settings for the coordinates picker module.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function coordinates_picker_menu() {
  $items = array();

  $items['admin/config/coordinates-picker-settings'] = array(
    'title' => 'Coordinates picker settings',
    'description' => 'Coordinates picker settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('coordinates_picker_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer coordinates_picker'),
    'weight' => 0,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'coordinates_picker.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_libraries_info().
 */
function coordinates_picker_libraries_info() {
  $libraries['locationpicker'] = array(
    'name' => 'JQuery Location Picker',
    'vendor url' => 'https://github.com/Logicify/jquery-locationpicker-plugin',
    'download url' => 'https://github.com/Logicify/jquery-locationpicker-plugin/archive/master.zip',
    'version arguments' => array(
      'file' => 'dist/locationpicker.jquery.min.js',
      'pattern' => '/v([0-9a-zA-Z\.-]+)/',
      'lines' => 10,
    ),
    'files' => array(
      'js' => array('dist/locationpicker.jquery.min.js'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function coordinates_picker_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  // Add a checkbox to the field settings form, to allow map picker.
  $instance = $form['#instance'];
  $field = $form['#field'];

  if ($field['type'] == 'geofield') {
    $field = $form['#field'];
    if (empty($form['instance']['settings'])) {
      $form['instance']['settings'] = array();
    }
    $form['instance']['settings'] += coordinates_picker_field_instance_settings_form($field, $instance);
  }
}

/**
 * Configuration form for editing insert settings for a field instance.
 *
 * @param array $field
 *   The $field array.
 * @param array $instance
 *   The $instance array.
 *
 * @return array
 *   A form element specific for our module's field instance configurations.
 */
function coordinates_picker_field_instance_settings_form($field, $instance) {

  $form['coordinates_picker'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable coordinates picker for this field.'),
    '#default_value' => !empty($instance['settings']['coordinates_picker']) ? $instance['settings']['coordinates_picker'] : FALSE,
    '#description' => t('This will add a link next to the text field to show a map and allow selecting the coordinates by draging the marker on the map.'),
    '#weight' => 12,
  );

  return $form;
}

/**
 * Implements hook_field_widget_form_alter().
 */
function coordinates_picker_field_widget_form_alter(&$element, &$form_state, $context) {
  // Add display_field setting to field because file_field_widget_form()
  // assumes it is set.
  $instance = $context['instance'];
  $settings = $instance['settings'];
  $field = $context['field'];
  if (!empty($settings['coordinates_picker']) && $field['type'] == 'geofield') {
    // @TODO Expand this to account also for the other widget types.
    if ($element['input_format']['#value'] == 'lat/lon') {
      $element['#process'][] = 'coordinates_picker_widget_process';
    }
  }
}

/**
 * Element #process callback for the geofield field type.
 *
 * Expands the input element to include the javascript and triggering link.
 *
 * @param array $element
 *   The $element array.
 * @param array $form_state
 *   The $form_state array.
 * @param array $form
 *   The $form array.
 *
 * @return array
 *   The element after processed by our function.
 */
function coordinates_picker_widget_process($element, &$form_state, $form) {
  $library = libraries_load('locationpicker');
  if (!empty($library['loaded'])) {

    // Expose the name of the geofield element and default lat/lon values.
    $id_geofield = $element['#id'] . '-geom';
    $default_lat = !empty($element['geom']['#default_value']['lat']) ? $element['geom']['#default_value']['lat'] : variable_get('coordinates_picker_default_lat');
    if (empty($default_lat)) {
      // When there is no defaults defined, the map will behave differently.
      $default_lat = 'default_zoom_out';
    }
    $default_lon = !empty($element['geom']['#default_value']['lon']) ? $element['geom']['#default_value']['lon'] : variable_get('coordinates_picker_default_lon');
    drupal_add_js(array(
      'coordinates_picker' => array(
        'id_geofield' => $id_geofield,
        'default_lat' => $default_lat,
        'default_lon' => $default_lon,
      ),
    ), 'setting');

    // Include the assets needed.
    drupal_add_js('https://maps.google.com/maps/api/js?libraries=places', 'external');
    drupal_add_js(drupal_get_path('module', 'coordinates_picker') . '/js/picker.js');

    // Add the additional link and placeholder for the map.
    $element['coordinates_picker_link'] = array(
      '#type' => 'markup',
      '#markup' => '<a id="coordinates-picker-link" href="#">' . t('Choose point in the map') . '</a>'
      . '<div id="coordinates-picker-map" style="width: 500px; height: 400px;"></div>',
    );
  }

  return $element;
}
