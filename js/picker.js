(function ($) {
  Drupal.behaviors.coordinatesPicker = {
    attach: function (context, settings) {

    $map = $('#coordinates-picker-map');

    // Initially hide the map.
    $map.addClass('element-invisible');

    $('#coordinates-picker-link').click(function(event) {
      $this = $(this);
      event.preventDefault();
      $map.removeClass('element-invisible');
      $this.addClass('element-invisible');

      // Initialize the map with some defaults.
      var id_geofield = Drupal.settings.coordinates_picker.id_geofield;
      if (Drupal.settings.coordinates_picker.default_lat == 'default_zoom_out') {
        var default_lat = 37.311106774252494;
        var default_lon = 3.2246935000000576;
        var default_zoom = 1;
      }
      else {
        var default_lat = Drupal.settings.coordinates_picker.default_lat;
        var default_lon = Drupal.settings.coordinates_picker.default_lon;
        var default_zoom = 8;
      }
      var $lat_element = $("#" + id_geofield + "-lat");
      var $lon_element = $("#" + id_geofield + "-lon");
      $map.locationpicker({
        location: {
          latitude: default_lat,
          longitude: default_lon
        },
        zoom: default_zoom,
        radius: 0,
        inputBinding: {
          latitudeInput: $lat_element,
          longitudeInput: $lon_element
        },
        enableAutocomplete: false,
        enableReverseGeocode: false
      });

    });
  
    }
  };
})(jQuery);