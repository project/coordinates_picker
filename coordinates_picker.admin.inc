<?php

/**
 * @file
 * Admin pages and settings forms for coordinates_picker module.
 */

/**
 * Form builder function for our module settings.
 *
 * @return array
 *   The form array to be rendered.
 */
function coordinates_picker_settings_form() {

  $form = array();

  $form['coordinates_picker_default_lat'] = array(
    '#type' => 'textfield',
    '#title' => t('Default latitude value.'),
    '#default_value' => variable_get('coordinates_picker_default_lat'),
    '#description' => t('Enter here the latitude value to be used as default when opening the map for the first time.'),
  );
  $form['coordinates_picker_default_lon'] = array(
    '#type' => 'textfield',
    '#title' => t('Default longitude value.'),
    '#default_value' => variable_get('coordinates_picker_default_lon'),
    '#description' => t('Enter here the longitude value to be used as default when opening the map for the first time.'),
  );

  return system_settings_form($form);

}
